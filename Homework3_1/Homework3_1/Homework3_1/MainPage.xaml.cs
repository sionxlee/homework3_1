﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework3_1
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async void OnCountDownTapped(object sender, System.EventArgs e)
        {
            bool usersResponse = await DisplayAlert("Count Down!",
                         "Start to count down from"+ CountDownFrom.Text.ToString()+ "?",
                         "Start!",
                         "Wait..");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new CountDown(int.Parse(CountDownFrom.Text.ToString())));
            }

        }
    }
}
