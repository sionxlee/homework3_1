﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CountDown : ContentPage
	{
		public CountDown (int count)
		{
			InitializeComponent ();
            RNumber.Text = count.ToString();
		}
        public CountDown(string context)
        {
            InitializeComponent();
            RNumber.Text = context;
            CountButton.IsVisible = false;
        }

        async void OnCountDownTapped(object sender, System.EventArgs e)
        {
            
            if (int.Parse(RNumber.Text.ToString()) == 1)
            {
                await Navigation.PushAsync(new CountDown("Finished!"));
            }
            else
            {
                await Navigation.PushAsync(new CountDown(int.Parse(RNumber.Text.ToString())-1));
            }

        }
    }
}